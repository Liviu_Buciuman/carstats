<?php
session_start();
include("header.html");
if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
    include("navbarSesion.html");
} else {
    include("navbar.html");
}
?>
<script src="js/bills.js"></script>

<body>
<div class="container">
    <div class="p-5"></div>
    <button type="button" class="btn btn-dark" id="export">
        <i class="far fa-folder-open"></i> Exportar a excel facturas diarias
    </button>

    <!--Tabla facturas-->
    <div class="p-3"></div>
    <div>
        <table id="datatable" class="display responsive nowrap">
            <thead>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            </thead>
            <tbody id="table">
            </tbody>
        </table>
    </div>
    <!--final-->
</div>

<!-- Modal boostrap para editar coches-->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="fas fa-edit"></i> Editar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="form-car" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="form-row ">
                            <div id="id2" style="display:none"></div>
                            <label for="select">Estado del pago</label>
                            <select id="ppp" class="custom-select col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <option id="o1"></option>
                                <option id="o2"></option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="btnEditPay">Aceptar</button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!--Fin modal-->


</body>