<?php
session_start();
include("header.html");
if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
    include("navbarSesion.html");
} else {
    include("navbar.html");
}
?>
<script type="text/javascript" src="js/usuarios.js"></script>

<div class="container">
    <div class="row">
        <div id="datosUsuario" class="col-6 mt-5"></div>
    </div>
    <div class="row">
        <div id="datosAlquiler" class="col-6"></div>
    </div>

    <div class="mt-3">
        <button id="btnEliminarCuenta" class="btn btn-danger"><i class="fas fa-trash"></i>
            Eliminar cuenta
        </button>
        <button id="botonEditarPasswdSesion" class="btn btn-primary" data-toggle="modal"
                data-target="#actualizarPasswdModal"><i
                    class="fas fa-key"></i>
            Cambiar contraseña
        </button>
        <button id='botonHistorialAlquiler' class='btn btn-info'><i class="fas fa-info-circle"></i> Ver historial
            alquiler
        </button>
    </div>

    <!--Tabla historial alquiler-->
    <div class="row">
        <div id="divHistorialAlquiler" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-5">
            <h3 class="border-bottom border-dark">Historial alquiler</h3>
            <div class="mt-5">
                <table id="datatable" class="display responsive nowrap">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="table">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal editar datos usuario-->
<div class="modal fade" id="editarDatosUsuarioSesion" tabindex="-1" role="dialog"
     aria-labelledby="editarDatosModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editarDatosModalLabel"><i class="fas fa-user-edit"></i> Editar datos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="editarDatosUsuarioModal" class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="submit" id="botonGuardarCambiosEditarDatosUsuarioSesion" class="btn btn-primary">Guardar
                    cambios
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal cambiar contraseña -->
<div class="modal fade" id="actualizarPasswdModal" tabindex="-1" role="dialog"
     aria-labelledby="actualizarPasswdModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="actualizarPasswdModalLabel"><i class="fas fa-pencil-alt"></i> Cambiar
                    contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div id="passwdOK" class="alert alert-success" role="alert">
                        Las contraseñas coinciden!
                    </div>
                    <div id="passwdNotOK" class="alert alert-warning" role="alert">
                        Las contraseñas <strong>no</strong> coinciden!
                    </div>
                    <div id="passwdEmptyDiv" class="alert alert-warning" role="alert">
                        El campo contraseña no puede estar vacío!
                    </div>
                    <div class="form-group">
                        <label for="actualPasswd" class="font-weight-bold">Introduzca contraseña actual</label>
                        <span id="requiredActualPasswd"></span>
                        <input type="password" id="actualPasswd" class="form-control" placeholder="Contraseña actual">
                    </div>
                    <div class="form-group">
                        <label for="nuevaPasswd" class="font-weight-bold">Nueva contraseña</label>
                        <span id="requiredNuevaPasswd"></span>
                        <input type="password" id="nuevaPasswd" class="form-control"
                               placeholder="Nueva contraseña">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" id="checkboxVerPasswd" class="form-check-input" onclick="verPasswd()">
                        <label for="checkboxVerPasswd" class="form-check-label"></label>
                        Ver nueva contraseña
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="botonGuardarPasswd" class="btn btn-primary">Guardar contraseña</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
