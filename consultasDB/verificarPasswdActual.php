<?php
session_start();

$host = "localhost";
$username = "root";
$passwd = "";
$dbname = "carstatsdb";

$conn = new mysqli("$host", "$username", "$passwd", "$dbname");
mysqli_set_charset($conn, "utf8");

$pass = $_POST["passwd"];
$id = $_SESSION["id"];

$query = $conn->query("SELECT * FROM cliente WHERE id = '$id'");

$passwdCorrecta = false;

if ($query->num_rows > 0) {
    $lista = array();
    $lista = $query->fetch_all(MYSQLI_ASSOC);
    foreach ($lista as $item => $value) {
        if (password_verify($pass, $value["passwd"])) {
            $passwdCorrecta = true;
        }
    }
}
$conn->close();
echo json_encode($passwdCorrecta, JSON_UNESCAPED_UNICODE);