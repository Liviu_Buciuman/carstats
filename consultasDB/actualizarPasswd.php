<?php
session_start();

$servername = "localhost";
$username = "root";
$passwd = "";
$dbname = "carstatsdb";

$conn = new mysqli($servername, $username, $passwd, $dbname);
mysqli_set_charset($conn, "utf8");

$pass = $_POST["passwd"];
$id = $_SESSION["id"];

$pass_hashed = password_hash($pass, PASSWORD_DEFAULT);

$sql = "UPDATE cliente SET passwd = '$pass_hashed' WHERE id = $id";

$passActualizada = false;

if ($conn->query($sql) === TRUE) {
    $passActualizada = true;
}

$conn->close();
echo json_encode($passActualizada, JSON_UNESCAPED_UNICODE);
