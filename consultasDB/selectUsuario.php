<?php
session_start();

$host = "localhost";
$username = "root";
$passwd = "";
$dbname = "carstatsdb";

$conn = new mysqli("$host", "$username", "$passwd", "$dbname");
mysqli_set_charset($conn, "utf8");

$dni = $_POST["dni"];
$pass = $_POST["passwd"];

$query = $conn->query("SELECT * FROM cliente WHERE dni = '$dni'");

$usuarioExiste = false;

if ($query->num_rows > 0) {
    $lista = array();
    $lista = $query->fetch_all(MYSQLI_ASSOC);
    foreach ($lista as $item => $value) {
        if (password_verify($pass, $value["passwd"])) {
            $_SESSION["id"] = $value["id"];
            $_SESSION["nombre"] = $value["nombre"];
            $usuarioExiste = true;
        }
    }
}
echo json_encode($usuarioExiste, JSON_UNESCAPED_UNICODE);
