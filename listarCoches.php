<?php
session_start();
include("header.html");
if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
    include("navbarSesion.html");
} else {
    include("navbar.html");
}
?>
<script type="text/javascript" src="js/correos.js"></script>
<script type="text/javascript" src="js/coches.js"></script>
<script type="text/javascript" src="js/usuarios.js"></script>

<!-- Script que guarda el id de la sesión -->
<script type="text/javascript">
    var idSesion;

    function actualiza() {
        idSesion = <?php
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            echo $_SESSION["id"];
        } else {
            echo -1;
        }
        ?>
    }
</script>

<div id="contenedorCoches" class="container mt-5">
    <div class="row">
        <div id="left" class="col">

        </div>
        <div id="right" class="col">

        </div>
    </div>
</div>

<?php include("footer.html"); ?>
