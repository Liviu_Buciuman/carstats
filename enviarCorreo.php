<?php
require_once 'PHPMailerAutoload.php';

$mensajeEnviado = "";
$nombre = $_POST["nombre"];
$email = $_POST["email"];
$texto = $_POST["texto"];
$titulo = $_POST["titulo"];

try {
    $mail = new PHPMailer(); // create a new object

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    $mail->IsSMTP(); // enable SMTP
    $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 465; // or 587
    $mail->IsHTML(true);
    $mail->Username = "carstatspuente@gmail.com";
    $mail->Password = "CarStats";
    $mail->setFrom("$email", "$nombre");
    $mail->addReplyTo("$email", "$nombre");
    $mail->Subject = "$titulo";
    $mail->Body = "$texto";
    $mail->AddAddress("carstatshelp@gmail.com");
    $mensajeEnviado = true;
    $mail->send();
} catch (phpmailerException $e) {
    $mensajeEnviado = false;
}
echo json_encode($mensajeEnviado, JSON_UNESCAPED_UNICODE);