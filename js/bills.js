$(document).ready(function () {
    var id = 0;
    tabla();

    $("#export").click(function () {
        exportExel();
    });

    $('#btnEditPay').click(function () {
        editPayment();
    });

});

//Llamada ajax para exportar a excel 
function exportExel() {
    $.ajax("consultasDB/dailyBills.php")
}

function tabla() {
    //Llamada ajax para cargar los datos de la base de datos en la tabla
    $.ajax("consultasDB/tableBills.php", {
        success: function (response) {
            //Destruyo la tabla antes de cargar una nueva(De lo contrario se concatenarian las tablas)
            $("#datatable").dataTable().fnDestroy();
            $('#table').empty();
            var bill = JSON.parse(response);
            for (var i = 0; i < bill.length; i++) {
                $('#table').append(
                    '<tr>' +
                    '<td>' + bill[i].dni + '</td>' +
                    '<td>' + bill[i].nombre + '</td>' +
                    '<td>' + bill[i].apellidos + '</td>' +
                    '<td>' + bill[i].marca + '-' + bill[i].modelo + '</td>' +
                    '<td>' + bill[i].fechaInicio + '-' + bill[i].fechaFin + '</td>' +
                    '<td>' + bill[i].estado + '</td>' +
                    '<td>' + bill[i].importe + ' €</td>' +
                    '<td> <button onClick="Edit(' + bill[i].id + ')" class="btn btn-warning" ><i class="fas fa-edit"></i>Editar</button></td>' +
                    '</tr>'
                );
                $()
            }
            //Plugin de jQuery DataTable
            table = $('#datatable').dataTable({
                "columnDefs": [
                    {"title": "DNI", "targets": 0},
                    {"title": "Nombre", "targets": 1},
                    {"title": "Apellidos", "targets": 2},
                    {"title": "Coche", "targets": 3},
                    {"title": "FechaReserva", "targets": 4},
                    {"title": "Estado", "targets": 5},
                    {"title": "Importe", "targets": 6},
                    {"title": "Editar", "targets": 7}
                ]
            });
        }
    });
}

function Edit(id) {

    $('#btnEditPay').val(id);

    data = {
        "id": id
    }
    $.ajax({
        data: data,
        url: 'consultasDB/paidStatus.php',
        type: 'GET',
        success: function (response) {
            var status = JSON.parse(response);
            if (status[0].estado == "pagado") {
                $('#o1').val("pagado");
                $('#o1').html("pagado");
                $('#o2').val("pendiente");
                $('#o2').html("pendiente");
            }
            if (status[0].estado == "pendiente") {
                $('#o1').val("pendiente");
                $('#o1').html("pendiente");
                $('#o2').val("pagado");
                $('#o2').html("pagado");
            }
        }
    });
    $('#ModalEdit').modal('toggle');
}

function editPayment() {

    data = {
        pay: $('#ppp option:selected').val(),
        id: $('#btnEditPay').val()
    }

    $.ajax({
        data: data,
        url: 'consultasDB/editPayment.php',
        type: 'POST',
        success: function (response) {
            tabla();
            $('#ModalEdit').modal('toggle');
            $.confirm({
                title: 'Hecho!',
                content: 'Pago editado',
                theme: 'black',
                type: 'orange',
                buttons: {
                    Ok: function () {
                    },
                }
            });
        }
    })
}