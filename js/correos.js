$(function () {
    $('#requiredDiv').hide();

    $('#btnEnviarMensaje').click(function () {
        enviarCorreo();
        // $('#contactarModal').hide();
    })
});

function validarCamposContactar() {
    $('#requiredDiv').show();
    if ($('#recipient-name').val() === "") {
        $('#requiredRecipientName').html('*').css('color', 'red');
    }
    if ($('#recipient-subject').val() === "") {
        $('#requiredRecipientSubject').html('*').css('color', 'red');
    }
    if ($('#recipient-mail').val() === "") {
        $('#requiredRecipientMail').html('*').css('color', 'red');
    }
    if ($('#message-text').val() === "") {
        $('#requiredMessageText').html('*').css('color', 'red');
    }
    $('#recipient-name').focusin(function () {
        $('#requiredRecipientName').html('');
        $('#requiredDiv').hide();
    });
    $('#recipient-mail').focusin(function () {
        $('#requiredRecipientMail').html('');
        $('#requiredDiv').hide();
    });
    $('#recipient-subject').focusin(function () {
        $('#requiredRecipientSubject').html('');
        $('#requiredDiv').hide();
    });
    $('#message-text').focusin(function () {
        $('#requiredMessageText').html('');
        $('#requiredDiv').hide();
    });
}

function enviarCorreo() {
    var nombre = $('#recipient-name').val();
    var email = $('#recipient-mail').val();
    var texto = $('#message-text').val();
    var titulo = $('#recipient-subject').val();

    if ($.trim(nombre).length > 0 && $.trim(email).length > 0 &&
        $.trim(texto).length > 0 && $.trim(titulo).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'enviarCorreo.php',
            data: {
                nombre: nombre,
                email: email,
                texto: texto,
                titulo: titulo
            },
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON === true) {
                    $.confirm({
                        icon: 'far fa-check-circle',
                        title: 'Correo enviado!',
                        content: 'El mensaje ha sido enviado!',
                        type: 'green',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-green',
                                action: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                } else {
                    $.confirm({
                        icon: 'fas fa-exclamation',
                        title: 'Upsss!!!',
                        content: 'El mensaje no ha sido enviado!',
                        type: 'orange',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-green',
                                action: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                }
            }
        })
    } else {
        validarCamposContactar();
    }
}
