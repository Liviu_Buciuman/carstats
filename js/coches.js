$(function () {
    getCoches();

    $('#listarCoches').click(function () {
        window.location = 'listarCoches.php';
    });
});

function alquilar(id) {
    actualiza();
    if (idSesion > -1) {
        //Elimino cookies
        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        //Creo cookie
        document.cookie = "id=" + idSesion;
        window.location = 'booking.php';
    } else {
        $.confirm({
            icon: 'fas fa-exclamation-triangle',
            title: '¡Inicia sesión!',
            content: 'Para alquilar coche inicia sesión o regístrate',
            type: 'orange',
            theme: 'modern',
            buttons: {
                Aceptar: {
                    btnClass: 'btn-orange'
                }
            }
        })
    }
}

function getCoches() {
    $.ajax({
        type: 'POST',
        url: 'consultasDB/selectCoches.php',
        success: function (data) {
            var dataJSON = JSON.parse(data);
            if (dataJSON.length > 0) {
                $.each(dataJSON, function (index) {
                    var imagenPortada =
                        "<div class='mt-5'>" +
                        "<a href='img/" + dataJSON[index].img1 + "' data-lightbox='galeria" + index + "'><img class='p-0 mx-auto d-block img-thumbnail hvr-forward' src='img/" + dataJSON[index].img0 + "'></a>" +
                        "<a href='img/" + dataJSON[index].img2 + "' data-lightbox='galeria" + index + "'></a>"
                        + "</div>";
                    var coches =
                        "<div class='border-bottom border-dark mt-5 text-justify' style='height: 273px; width: 540px;'>" +
                        "<button onclick='alquilar(" + dataJSON[index].id + ")' class='btn btn-primary mr-2' value='" + dataJSON[index].id + "'>Alquilar</button>" +
                        "<strong>" + dataJSON[index].marca + " " + dataJSON[index].modelo + "</strong>" + ", " + dataJSON[index].precio + " €/día" +
                        "<p class='mt-3'>" + dataJSON[index].descripcion + "</p>" +
                        "<strong>Motor: </strong>" + dataJSON[index].motor +
                        "<strong> Transmisión: </strong>" + dataJSON[index].transmision +
                        "<strong> Puertas: </strong>" + dataJSON[index].puertas +
                        "<strong> Plazas: </strong>" + dataJSON[index].plazas + "<br>" +
                        "<strong> Categoria: </strong>" + dataJSON[index].categoria + "<br><br>"
                        + "</div>";
                    $('#left').append(imagenPortada);
                    $('#right').append(coches);
                });
            } else {
                alert("No existen coches");
            }

        },
        error: function (request, errorType, errorMessage) {
            alert('Error ' + errorType + ' Mensaje: ' + errorMessage);
        }
    })
}