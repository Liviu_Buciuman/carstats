$(function () {

    selectDatosUsuarioSesion();
    editarDatosUsuarioSesion();

    $('#divHistorialAlquiler').hide();
    $('#passwdOK').hide();
    $('#passwdNotOK').hide();
    $('#passwdEmptyDiv').hide();

    $('#aceptar').click(function () {
        verificarSiExisteDni();
    });

    $('#iniciarSesion').click(function () {
        selectUsuario();
    });

    $('#botonGuardarCambiosEditarDatosUsuarioSesion').click(function () {
        insertarDatosUsuarioModificados();
        $('#editarDatosUsuarioSesion').modal('hide');
    });

    $('#botonHistorialAlquiler').click(function () {
        $('#divHistorialAlquiler').toggle();
    });

    $('#actualPasswd').focusout(function () {
        consultaPasswordActualDB();
    });

    $('#botonGuardarPasswd').click(function () {
        actualizarPasswd();
        $('#actualizarPasswdModal').modal('hide');
    });

    $('#btnEliminarCuenta').click(function () {
        eliminarCuenta();
    })
});

function eliminarCuenta() {
    $.ajax({
        type: 'POST',
        url: 'consultasDB/delete_user.php',
        success: function (data) {
            var dataJSON = JSON.parse(data);
            if (dataJSON === true) {
                $.confirm({
                    icon: 'far fa-check-circle',
                    title: '¡Cuenta eliminada!',
                    content: 'Su cuenta ha sido eliminada. Algunos datos permanecerán guardados.',
                    type: 'green',
                    theme: 'modern',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn-green',
                            action: function () {
                                window.location.replace('consultasDB/cerrarSesion.php');
                            }
                        }
                    }
                });
            } else {
                $.confirm({
                    icon: 'far fa-check-circle',
                    title: '¡Cuenta no eliminada!',
                    content: 'Su cuenta no ha sido eliminada.',
                    type: 'orange',
                    theme: 'modern',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn-orange',
                            action: function () {
                                window.location.replace('consultasDB/cerrarSesion.php');
                            }
                        }
                    }
                });
            }
        }
    })
}

function insertarDatosUsuarioModificados() {
    var dniModificado = $('#dniModificado').val();
    var nombreModificado = $('#nombreModificado').val();
    var apellidosModificado = $('#apellidosModificado').val();
    var direccionModificado = $('#direccionModificado').val();
    var telModificado = $('#telModificado').val();
    var emailModificado = $('#emailModificado').val();

    if ($.trim(dniModificado).length > 0 && $.trim(nombreModificado).length > 0 && $.trim(apellidosModificado).length > 0
        && $.trim(telModificado).length > 0 && $.trim(direccionModificado).length > 0
        && $.trim(emailModificado).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'consultasDB/insertarDatosModificadosUsuario.php',
            data: {
                dni: dniModificado,
                nombre: nombreModificado,
                apellidos: apellidosModificado,
                tel: telModificado,
                direccion: direccionModificado,
                email: emailModificado
            },
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON === true) {
                    $.confirm({
                        icon: 'far fa-check-circle',
                        title: 'Datos modificados!',
                        content: 'Sus datos han sido guardados!',
                        type: 'green',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-green',
                                action: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                } else {
                    $.confirm({
                        icon: 'fas fa-times',
                        title: '¡Datos no modificados!',
                        content: '¡Sus datos no se han guardado!',
                        type: 'red',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-red'
                            }
                        }
                    });
                }
            }
        })
    } else {
        $.confirm({
            icon: 'fas fa-times',
            title: '¡Datos no modificados!',
            content: '¡No se pueden dejar campos vacíos!',
            type: 'red',
            theme: 'modern',
            buttons: {
                Aceptar: {
                    btnClass: 'btn-red',
                    action: function () {
                        location.reload();
                    }
                }
            }
        });
    }
}

function editarDatosUsuarioSesion() {
    $.ajax({
        url: 'consultasDB/selectUsuarioSesion.php',
        success: function (data) {
            var dataJSON = JSON.parse(data);
            $.each(dataJSON, function (index) {
                $('#editarDatosUsuarioModal').append(
                    "<label for='dniModificado'>DNI:</label>" +
                    "<input id='dniModificado' class='form-control' type='text' value='" + dataJSON[index].dni + "' placeholder='DNI'>" +
                    "<form class='form-group'>" +
                    "<label for='nombreModificado'>Nombre:</label>" +
                    "<input id='nombreModificado' class='form-control' type='text' value='" + dataJSON[index].nombre + "' placeholder='Nombre'>" +
                    "<label for='apellidosModificado'>Apellidos:</label>" +
                    "<input id='apellidosModificado' class='form-control' type='text' value='" + dataJSON[index].apellidos + "' placeholder='Apellidos'>" +
                    "<label for='direccionModificado'>Dirección:</label>" +
                    "<input id='direccionModificado' class='form-control' type='text' value='" + dataJSON[index].direccion + "' placeholder='Dirección'>" +
                    "<label for='telModificado'>Teléfono:</label>" +
                    "<input id='telModificado' class='form-control' type='text' value='" + dataJSON[index].telefono + "' placeholder='Teléfono'>" +
                    "<label for='emailModificado'>Email:</label>" +
                    "<input id='emailModificado' class='form-control' type='email' value='" + dataJSON[index].email + "' placeholder='Email'>" +
                    "</form>"
                )
            })
        },
        error: function (request, errorType, errorMessage) {
            alert('Error' + errorType + 'Mensaje: ' + errorMessage);
        }
    });
}

function selectDatosUsuarioSesion() {
    $.ajax({
        url: 'consultasDB/selectUsuarioSesion.php',
        success: function (data) {
            var dataJSON = JSON.parse(data);
            $.each(dataJSON, function (index) {
                var datosUsuario =
                    "<div class='border-bottom border-dark'>" +
                    "<button id='botonEditarDatosUsuarioSesion' class='btn btn-warning mb-3' data-toggle='modal' " +
                    "data-target='#editarDatosUsuarioSesion'><i class='fas fa-user-edit'></i> Editar datos</button>" +
                    "<h3 class='border-bottom border-dark border-top border-danger'>" + dataJSON[index].apellidos + ", " + dataJSON[index].nombre + "</h3>" +
                    "<p class='mt-3'>" + "<strong>DNI: </strong>" + dataJSON[index].dni + "</p>" +
                    "<p>" + "<strong>Dirección: </strong>" + dataJSON[index].direccion + "</p>" +
                    "<p>" + "<strong>Teléfono: </strong>" + dataJSON[index].telefono + "</p>" +
                    "<p>" + "<strong>Email: </strong>" + dataJSON[index].email + "</p>" +
                    "</div>";
                $('#datosUsuario').append(datosUsuario);
                services(dataJSON[index].id);
            })
        },
        error: function (request, errorType, errorMessage) {
            alert('Error' + errorType + 'Mensaje: ' + errorMessage);
        }
    });
}

function verificarSiExisteDni() {
    var dni = $("#dni").val();
    if ($.trim(dni).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'consultasDB/verificarDni.php',
            data: {dni: dni},
            success: function (data) {
                if (data === "true") {
                    $("#requiredAll").html("El DNI ya existe");
                } else {
                    insertarUsuario();
                }
            }
        })
    } else {
        validarFormulario(dni);
    }
}

function validarFormulario(dni, nombre, apellidos, pass, tel, direccion, email) {
    if (dni === "") {
        $("#requiredAll").html("* El campo DNI no puede estar vacío");
        $("#requiredDni").html("*").css("color", "red");
        $("#dni").focusin(function () {
            $("#requiredDni").html("");
            $("#requiredAll").html("");
        })
    } else if (nombre === "") {
        $("#requiredAll").html("* El campo Nombre no puede estar vacío");
        $("#requiredNombre").html("*").css("color", "red");
        $("#nombre").focusin(function () {
            $("#requiredNombre").html("");
            $("#requiredAll").html("");
        })
    } else if (apellidos === "") {
        $("#requiredAll").html("* El campo Apellidos no puede estar vacío");
        $("#requiredApellidos").html("*").css("color", "red");
        $("#apellidos").focusin(function () {
            $("#requiredApellidos").html("");
            $("#requiredAll").html("");
        })
    } else if (pass === "") {
        $("#requiredAll").html("* El campo Contraseña no puede estar vacío");
        $("#requiredPass").html("*").css("color", "red");
        $("#pass").focusin(function () {
            $("#requiredPass").html("");
            $("#requiredAll").html("");
        })
    } else if (tel === "") {
        $("#requiredAll").html("* El campo Teléfono no puede estar vacío");
        $("#requiredTel").html("*").css("color", "red");
        $("#tel").focusin(function () {
            $("#requiredTel").html("");
            $("#requiredAll").html("");
        })
    } else if (direccion === "") {
        $("#requiredAll").html("* El campo Dirección no puede estar vacío");
        $("#requiredDire").html("*").css("color", "red");
        $("#dire").focusin(function () {
            $("#requiredDire").html("");
            $("#requiredAll").html("");
        })
    } else if (email === "") {
        $("#requiredAll").html("* El campo Correo electrónico no puede estar vacío");
        $("#requiredEmail").html("*").css("color", "red");
        $("#email").focusin(function () {
            $("#requiredEmail").html("");
            $("#requiredAll").html("");
        })
    }
}

function insertarUsuario() {
    var dni = $("#dni").val();
    var nombre = $("#nombre").val();
    var apellidos = $("#apellidos").val();
    var pass = $("#pass").val();
    var tel = $("#tel").val();
    var direccion = $("#dire").val();
    var email = $("#email").val();
    if ($.trim(dni).length > 0 && $.trim(nombre).length > 0 && $.trim(apellidos).length > 0
        && $.trim(pass).length > 0 && $.trim(tel).length > 0 && $.trim(direccion).length > 0
        && $.trim(email).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'consultasDB/insertarUsuario.php',
            data: {
                dni: dni,
                nombre: nombre,
                apellidos: apellidos,
                pass: pass,
                tel: tel,
                direccion: direccion,
                email: email
            },
            success: function (data) {
                if (data === "true") {
                    $('#exampleModalCenter').modal("hide");
                    limpiarFormulario();
                    $.confirm({
                        icon: 'far fa-check-circle',
                        title: '¡Usuario registrado!',
                        content: 'Puedes iniciar sesión.',
                        type: 'green',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-green'
                            }
                        }
                    });
                } else {
                }
            }

        })
    } else {
        validarFormulario(dni, nombre, apellidos, pass, tel, direccion, email);
    }
}

function validarLogin(dni, passwd) {
    if (dni === "") {
        $("#required").html("El campo DNI no puede estar vacío");
        $("#dniSesion").focusin(function () {
            $("#required").html("");
        })
    } else if (passwd === "") {
        $("#required").html("El campo Contraseña no puede estar vacío");
        $("#passwdSesion").focusin(function () {
            $("#required").html("");
        })
    }
}

function selectUsuario() {
    var dni = $("#dniSesion").val();
    var passwd = $("#passwdSesion").val();

    if ($.trim(dni).length > 0 && $.trim(passwd).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'consultasDB/selectUsuario.php',
            data: {dni: dni, passwd: passwd},
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON) {
                    window.location.href = "listarCoches.php";
                } else {
                    $("#required").html("Datos incorrectos!");
                }
            }
        })
    } else {
        validarLogin(dni, passwd);
    }

}

function limpiarFormulario() {
    $("#dni").val("");
    $("#nombre").val("");
    $("#apellidos").val("");
    $("#pass").val("");
    $("#tel").val("");
    $("#dire").val("");
    $("#email").val("");
    $("#requiredAll").val("");
}

function services(id) {
    //Llamada ajax para cargar los datos de la base de datos en la tabla
    $.ajax({
        data: {id: id},
        url: 'admin/modelos/usuarios/service.php',
        type: 'GET',
        success: function (response) {
            var user = JSON.parse(response);
            for (var i = 0; i < user.length; i++) {
                $('#table').append(
                    '<tr>' +
                    '<td>' + user[i].fechainicio + '</td>' +
                    '<td>' + user[i].fechafin + '</td>' +
                    '<td>' + user[i].marca + '</td>' +
                    '<td>' + user[i].modelo + '</td>' +
                    '<td>' + user[i].matricula + '</td>' +
                    '</tr>'
                );
            }
            //Plugin de jQuery DataTable
            table = $('#datatable').dataTable({
                "columnDefs": [
                    {"title": "FechaInicio", "targets": 0},
                    {"title": "FechaFin", "targets": 1},
                    {"title": "Marca", "targets": 2},
                    {"title": "Modelo", "targets": 3},
                    {"title": "Matricula", "targets": 4}
                ]
            });
        }
    });
}

function verPasswd() {
    var nuevaPasswd = document.getElementById("nuevaPasswd");
    if (nuevaPasswd.type === "password") {
        nuevaPasswd.type = "text";
    } else {
        nuevaPasswd.type = "password";
    }
}

function verificarPasswdActual() {
    $('#passwdEmptyDiv').show();
    $("#requiredActualPasswd").html("*").css("color", "red");
    $('#actualPasswd').focusin(function () {
        $('#passwdEmptyDiv').hide();
        $('#passwdOK').hide();
        $('#passwdNotOK').hide();
        $("#requiredActualPasswd").html("");
    });
}

function consultaPasswordActualDB() {
    var actualPasswd = $('#actualPasswd').val();
    if ($.trim(actualPasswd).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'consultasDB/verificarPasswdActual.php',
            data: {
                passwd: actualPasswd
            },
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON === true) {
                    $('#passwdOK').show();
                    $('#passwdNotOK').hide();
                } else {
                    $('#passwdNotOK').show();
                    $('#passwdOK').hide();
                }
            }
        })
    } else {
        verificarPasswdActual();
    }
}

function verificarPasswdNueva(nuevaPasswd) {
    $('#passwdEmptyDiv').show();
    $("#requiredNuevaPasswd").html("*").css("color", "red");
    nuevaPasswd.focusin(function () {
        $('#passwdEmptyDiv').hide();
        $('#passwdOK').hide();
        $('#passwdNotOK').hide();
        $("#requiredNuevaPasswd").html("");
    });
}

function actualizarPasswd() {
    var actualPasswd = $('#actualPasswd');
    var nuevaPasswd = $('#nuevaPasswd');
    var nuevaPasswdVal = nuevaPasswd.val();
    var actualPasswdVal = actualPasswd.val();

    if ($.trim(actualPasswdVal).length === 0) {
        verificarPasswdActual();
    }

    if ($.trim(nuevaPasswdVal).length > 0) {
        $.ajax({
            type: 'POST',
            url: 'consultasDB/actualizarPasswd.php',
            data: {
                passwd: nuevaPasswdVal
            },
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON === true) {
                    $.confirm({
                        icon: 'far fa-check-circle',
                        title: 'Contraseña actualizada!',
                        content: 'Su contraseña ha sido actualizada!',
                        type: 'green',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-green',
                                action: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                }
            }
        })
    } else {
        verificarPasswdNueva(nuevaPasswd);
    }
}
