$(document).ready(function () {
    $('#datatable').DataTable();
    tabla();

    $("#btnNuevoUsuario").click(function () {
        addUser();
    });
    $("#btnEditUsuario").click(function () {
        Edit2();
    });

});

function tabla() {
    //Llamada ajax para cargar los datos de la base de datos en la tabla
    $.ajax("../modelos/usuarios/list_user.php", {
        success: function (response) {
            //Destruyo la tabla antes de cargar una nueva(De lo contrario se concatenarian las tablas)
            $("#datatable").dataTable().fnDestroy();
            $('#table').empty();
            var user = JSON.parse(response);
            for (var i = 0; i < user.length; i++) {
                $('#table').append(
                    '<tr>' +
                    '<td>' + user[i].dni + '</td>' +
                    '<td>' + user[i].nombre + '</td>' +
                    '<td>' + user[i].apellidos + '</td>' +
                    '<td> <button onClick="View(' + user[i].id + ')" class="btn btn-info"><i class="fas fa-street-view"></i>Ver</button></td>' +
                    '<td> <button onClick="Edit(' + user[i].id + ')" class="btn btn-warning"><i class="fas fa-edit"></i>Editar</button></td>' +
                    '<td> <button onClick="Reservar(' + user[i].id + ')" class="btn btn-success"><i class="fas fa-briefcase"></i> Reservar </button></td>' +
                    '<td> <button onClick="Delete(' + user[i].id + ')" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Eliminar</button></td>' +
                    '</tr>'
                );
            }
            //Plugin de jQuery DataTable
            table = $('#datatable').dataTable({
                "columnDefs": [
                    {"title": "DNI", "targets": 0},
                    {"title": "Nombre", "targets": 1},
                    {"title": "Apellido", "targets": 2},
                    {"title": "Ver", "targets": 3},
                    {"title": "Editar", "targets": 4},
                    {"title": "Reservar", "targets": 5},
                    {"title": "Eliminar", "targets": 6}
                ]
            });
        }
    });
}

//Envia a la vista de datos del usuario
function View(id) {
    //Elimino cookies
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    //Creo cookie
    document.cookie = "id=" + id;
    window.location.href = "../vistas/user.php";
}

//Agregar usuario
function addUser() {
    var dni = $('#dni').val();
    var nom = $('#nombre').val();
    var ape = $('#apellidos').val();
    var pass = $('#pass').val();
    var dire =  $('#dire').val();
    var tel =  $('#tel').val();
    var email =  $('#email').val();
    if (dni.trim().length>0 && nom.trim().length>0  && ape.trim().length>0 && pass.trim().length>0 && dire.trim().length>0 && tel.trim().length>0 && email.trim().length>0){

        var datos = {
            "dni": $('#dni').val(),
            "nom": $('#nombre').val(),
            "ape": $('#apellidos').val(),
            "pass": $('#pass').val(),
            "dire": $('#dire').val(),
            "tel": $('#tel').val(),
            "email": $('#email').val()
        }

        $.ajax({
            data: datos,
            url: '../modelos/usuarios/add_user.php',
            type: 'POST',
            success: function (response) {
                tabla();
                $('#ModalAdd').modal('toggle');
                $.confirm({
                    title: 'Hecho!',
                    content: 'Usuario creado',
                    theme: 'black',
                    type: 'orange',
                    buttons: {
                        Ok: function () {
                        },
                    }
                });
            }
        })
    }else{
        $('#error').html("*Rellene todo los campos")
        $("#error").css("color", "red");
    }
}

//Borrar usuario
function Delete(id) {
    var id = {
        "id": id,
    }
    $.confirm({
        title: 'Confirma!',
        content: '¿!Deseas BORRAR a este usuario!?',
        theme: 'Supervan',
        buttons: {
            confirmar: function () {
                $.ajax({
                    data: id,
                    url: '../modelos/usuarios/delete_user.php',
                    type: 'get',
                    success: function (response) {
                        if (response > 0) {
                            tabla();
                        }
                        else {
                            alert("Error al borrar usuario");
                        }
                    }
                });
            },
            cancelar: function () {
            },
        }
    });
}

//Cargo datos en Modal de editar y lo abro
function Edit(id) {
    data = {
        "id": id
    }
    $.ajax({
        data: data,
        url: '../modelos/usuarios/view_user.php',
        type: 'GET',
        success: function (response) {
            var user = JSON.parse(response);
            for (var i = 0; i < user.length; i++) {
                if (user[i].id == id) {
                    $('#iduser').val(user[i].id);
                    $('#dni2').val(user[i].dni);
                    $('#nombre2').val(user[i].nombre);
                    $('#apellidos2').val(user[i].apellidos);
                    $('#tel2').val(user[i].telefono);
                    $('#dire2').val(user[i].direccion);
                    $('#email2').val(user[i].email);
                }
            }
        }
    });
    $('#ModalEdit').modal('toggle');
    tabla();
}

//Actualizo la base de datos 
function Edit2() {
    var datos = {
        "id": $('#iduser').val(),
        "dni": $('#dni2').val(),
        "nom": $('#nombre2').val(),
        "ape": $('#apellidos2').val(),
        "dire": $('#dire2').val(),
        "tel": $('#tel2').val(),
        "email": $('#email2').val()
    }
    $.ajax({
        data: datos,
        url: '../modelos/usuarios/edit_user.php',
        type: 'POST',
        success: function (response) {
            tabla();

            $('#ModalEdit').modal('toggle');
            $.confirm({
                title: 'Hecho!',
                content: 'Usuario editado',
                theme: 'black',
                type: 'orange',
                buttons: {
                    Ok: function () {
                    },
                }
            });
        }
    })
}

//Redirect a la pagina de reservas/facturacion
function Reservar(id) {

    //Elimino cookies
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    //Creo cookie
    document.cookie = "id=" + id;
    window.location.href = "../vistas/booking.php";
}


