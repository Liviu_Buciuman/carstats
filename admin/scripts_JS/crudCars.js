$(document).ready(function(){
    tabla();

    $( "#btnNewCar" ).click(function() {
        addCar();
    });

    $( "#btnEditCar ").click(function(){
        edit2();
    })
});

function tabla(){
    //Llamada ajax para cargar los datos de la base de datos en la tabla
    $.ajax("../modelos/coches/list_cars.php", {
        success:function(response){
            //Destruyo la tabla antes de cargar una nueva(De lo contrario se concatenarian las tablas)
            $("#datatable").dataTable().fnDestroy();
            $('#table').empty();
            var car = JSON.parse(response);
            for(var i=0; i<car.length; i++){
                $('#table').append(
                    '<tr>'+
                        '<td>' + car[i].marca + '</td>'+
                        '<td>' + car[i].modelo + '</td>'+
                        '<td> <button onClick="View('+car[i].id+')" class="btn btn-info"><i class="fas fa-street-view"></i>Ver</button></td>'+
                        '<td> <button onClick="Edit('+car[i].id+')" class="btn btn-warning"><i class="fas fa-edit"></i>Editar</button></td>'+
                        '<td> <button onClick="Delete('+car[i].id+')" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Eliminar</button></td>'+
                        '<td>' + car[i].matricula + '</td>'+

                    '</tr>'
                );
            }
            //Plugin de jQuery DataTable
            table = $('#datatable').dataTable( {            
                "columnDefs": [
                    { "title": "Marca", "targets": 0 },
                    { "title": "Modelo", "targets": 1 },
                    { "title": "Ver", "targets": 2 },
                    { "title": "Editar", "targets": 3 },
                    { "title": "Elimiar", "targets": 4 },
                    { "title": "Matricula", "targets": 5 }

                ]
            });
        }  
    });
}
//Envia a la vista de datos del coche
function View(id){
    //Elimino cookies
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    //Creo cookie
    document.cookie = "id="+id;
    window.location.href = "../vistas/car.php";
}

//Agregamos coche nuevo
function addCar(){
    var marca = $("#marca").val();
    var modelo = $("#modelo").val();
    var categoria = $("#categoria").val();
    var motor = $("#motor").val();
    var plazas = $("#plazas").val();
    var puertas = $("#puertas").val();
    var matricula = $("#matricula").val();
    var trans = $("#trans").val();
    var descripcion = $("#descripcion").val();
    var precio = $("#precio").val();

    var inputFile0 = $("#img0")[0];
    var inputFile1 = $("#img1")[0];
    var inputFile2 = $("#img2")[0];

    if (marca.trim().length>0 && modelo.trim().length>0 && categoria.trim().length>0 && motor.trim().length>0 && plazas.trim().length>0 && puertas.trim().length>0 && matricula.trim().length>0 
    && trans.trim().length>0 && descripcion.trim().length>0 && precio.trim().length>0  && $("#img0").val() && $("#img1").val() && $("#img2").val()){

        var data = new FormData();
        data.append('img0', inputFile0.files[0]);
        data.append('img1', inputFile1.files[0]);
        data.append('img2', inputFile2.files[0]);

        data.append('marca', marca);
        data.append('modelo', modelo);
        data.append('categoria', categoria);
        data.append('motor', motor);
        data.append('puertas', puertas);
        data.append('plazas', plazas);
        data.append('matricula', matricula);
        data.append('trans', trans);
        data.append('descripcion', descripcion);
        data.append('precio', precio);

        $.ajax({
            data : data,
            url  : '../modelos/coches/add_car.php',
            type : 'POST',
            contentType: false,
            cache: false,
            processData:false,
            success: function (response){
                tabla();
                $('#ModalAdd').modal('toggle');
                    $.confirm({
                        title: 'Hecho!',
                        content: 'Coche añadido',
                        theme: 'black',
                        type: 'orange',
                        buttons: {
                            Ok: function () {
                            },
                        }
                    });
            }
        })
    }else{
        $('#error').html("*Rellene todo los campos")
        $("#error").css("color", "red");
    }
}    
//Borrar usuario
function Delete(id){
    var id ={
        "id" : id,
    }
    $.confirm({
        title: 'Confirma!',
        content: '¿!Deseas BORRAR a este coche!?',
        theme: 'Supervan',
        buttons: {
            confirmar: function () {

                $.ajax({
                    data: id, 
                    url: '../modelos/coches/delete_car.php',
                    type: 'get',
                    success: function(response){
                        if (response>0){
                            tabla();
                        }
                        else{
                            tabla();
                        }
                    }
                });            
            },
            cancelar: function () {
            },
        }
    });
}
//Cargamos datos en modal para editar
function Edit(id){
    data = {
        "id" : id
    }
    $.ajax({
        data : data,
        url  : '../modelos/coches/view_car.php',
        type : 'GET',
        success:function(response){      
            var car = JSON.parse(response);
            for(var i=0; i<car.length; i++){
                if(car[i].id==id){
                    //Cargo datos en el form de editar
                    $('#id2').val(car[i].id);
                    $('#marca2').val(car[i].marca);
                    $('#modelo2').val(car[i].modelo);
                    $('#categoria2').val(car[i].categoria);
                    $('#motor2').val(car[i].motor);
                    $('#plazas2').val(car[i].plazas);
                    $('#puertas2').val(car[i].puertas);
                    $('#matricula2').val(car[i].matricula);
                    $('#trans2').val(car[i].transmision);
                    $('#descripcion2').val(car[i].descripcion);
                    $('#precio2').val(car[i].precio);

                }
            }
        }
    });
    $('#ModalEdit').modal('toggle');
    tabla();
}

function edit2(){
    
    var data ={ 
        "id" : $('#id2').val(),
        "marca" : $("#marca2").val(),
        "modelo" : $("#modelo2").val(),
        "categoria" : $("#categoria2").val(),
        "motor" : $("#motor2").val(),
        "plazas" : $("#plazas2").val(),
        "puertas" : $("#puertas2").val(),
        "matricula" : $("#matricula2").val(),
        "trans" : $("#trans2").val(),
        "precio" : $("#precio2").val(),
        "descripcion" : $("#descripcion2").val()
    }

    $.ajax({
        data : data,
        url  : '../modelos/coches/edit_car.php',
        type : 'POST',
        success: function (response){
            tabla();
            $('#ModalEdit').modal('toggle');
                $.confirm({
                    title: 'Hecho!',
                    content: 'Coche editado',
                    theme: 'black',
                    type: 'orange',
                    buttons: {
                        Ok: function () {
                        },
                    }
                });
        }
     })
}

function borrarImg(div, img, id){
    data = {
        "id" : id,
        "img" : img,
    }
    $.ajax({
        data : data,
        url  : '../modelos/coches/delete_img.php',
        type : 'POST',
        success:function(response){
            //Dependiendo del boton presionado cargo su input corresponiente, despues de borrar la imagen
            if(div=='aux0'){
                $('#aux0').html('<input type="file" accept="image/*" class="form-control col-form-label-sm" id="img4" required></div>');
            }
            if(div=='aux1'){
                $('#aux1').html('<input type="file" accept="image/*" class="form-control col-form-label-sm" id="img5" required></div>');
            }
            if(div=='aux2'){
                $('#aux2').html('<input type="file" accept="image/*" class="form-control col-form-label-sm" id="img6" required></div>');
            }
        }
    });
}