$(document).ready(function () {
    var x = document.cookie;
    y = x.split("=")
    user(y[1]);
    services(y[1])
});

//Cargamos datos personales
function user(id) {
    data = {
        "id": id
    }
    $.ajax({
        data: data,
        url: '../modelos/usuarios/view_user.php',
        type: 'GET',
        success: function (response) {
            var user = JSON.parse(response);
            $("#nombre").html(user[0].nombre);
            $("#apellido").html(user[0].apellidos);
            $("#dni").html(user[0].dni);
            $("#direccion").html(user[0].direccion);
            $("#telefono").html(user[0].telefono);
            $("#email").html(user[0].email);
            $("#fecha").html(user[0].fechaRegistro);
        }
    })
}

//Cargamos lo servicios en la tabla
function services(id) {
    //Llamada ajax para cargar los datos de la base de datos en la tabla
    data = {
        "id": id
    };
    $.ajax({
        data: data,
        url: '../modelos/usuarios/service.php',
        type: 'GET',
        success: function (response) {
            var user = JSON.parse(response);
            for (var i = 0; i < user.length; i++) {
                $('#table').append(
                    '<tr>' +
                    '<td>' + user[i].fechainicio + '</td>' +
                    '<td>' + user[i].fechafin + '</td>' +
                    '<td>' + user[i].marca + '</td>' +
                    '<td>' + user[i].modelo + '</td>' +
                    '<td>' + user[i].matricula + '</td>' +
                    '</tr>'
                );
            }

            //Plugin de jQuery DataTable



            table = $('#datatable').dataTable( {
                "columnDefs": [
                    {"title": "FechaInicio", "targets": 0},
                    {"title": "FechaFin", "targets": 1},
                    {"title": "Marca", "targets": 2},
                    {"title": "Modelo", "targets": 3},
                    {"title": "Matricula", "targets": 4}
                ]
            });
        }
    });
}

function goBack() {
    window.history.back();
}