$(document).ready(function(){

    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    var idCar =unescape(value[1]);
    $('#idCar').val(idCar);
    car(idCar);

    $(".cb").colorbox({rel:'cb'});

    //Botones pasa subir imagenes
    $( "#btn0" ).click(function() {
        upImg(1);
    });
    $( "#btn1" ).click(function() {
        upImg(2);
    });
    $( "#btn2" ).click(function() {
        upImg(3);
    });

});

function goBack() {
    window.history.back();
}

//Cargamos datos del coche
function car(id){


    var ruta = "../../img/"
    data = {
        "id" : id
    }
    $.ajax({
        data : data,
        url  : '../modelos/coches/view_car.php',
        type : 'GET',
        success: function (response){
            var car = JSON.parse(response);
            $("#marca").html(car[0].marca);
            $("#modelo").html(car[0].modelo);
            $("#categoria").html(car[0].categoria);
            $("#motor").html(car[0].motor);
            $("#plazas").html(car[0].plazas);
            $("#puertas").html(car[0].puertas);
            $("#matricula").html(car[0].matricula);
            $("#trans").html(car[0].transmision);
            $("#descripcion").html(car[0].descripcion);
            $("#precio").html(car[0].precio);

            if(car[0].img0.length>0){
                img0 = car[0].img0;
            }else{
                img0 = "no_image.jpg";
            }

            if(car[0].img1.length>0){
                img1 = car[0].img1;
            }else{
                img1 = "no_image.jpg";
            }
            if(car[0].img2.length>0){
                img2 = car[0].img2;
            }else{
                img2 = "no_image.jpg";
            }

            $("#img0").attr("src", ruta + img0);

            $("#img0").attr("href", ruta + img1);

            $("#img2").attr("href", ruta + img2);

            //Imagen portada
            if(car[0].img0!=""){
                $('#aux0').html('<button onClick="borrarImg(\'aux0\', \'img0\' ,\''+car[0].id+'\')" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Eliminar Imagen portada</button>');
                $('#aux0').val(car[0].img0);
            }else{
                $('#aux0').append('<input type="file" accept="image/*" class="form-control" aria-describedby="basic-addon2" id="img4">');
                $('#btn0').append('<button id="btn0" class="btn btn-success" type="button">Subir</button>')

            }
            //Imagen 1
            if(car[0].img1!=""){
                $('#aux1').html('<button onClick="borrarImg(\'aux1\', \'img1\' ,\''+car[0].id+'\')" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Eliminar segunda imagen</button>');
                $('#aux1').val(car[0].img1);
            }else{
                $('#aux1').append('<input type="file"  accept="image/* "class="form-control" aria-describedby="basic-addon2" id="img5"">');
                $('#btn1').append('<button id="btn1" class="btn btn-success" type="button">Subir</button>')
            }
            //Imagen 2
            if(car[0].img2!=""){
                $('#aux2').html('<button onClick="borrarImg(\'aux2\', \'img2\' ,\''+car[0].id+'\')" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Eliminar tercera imagen</button>');
                $('#aux2').val(car[0].img2);
            }else{
                $('#aux2').append('<input type="file"  accept="image/* "class="form-control" aria-describedby="basic-addon2" id="img6">');
                $('#btn2').append('<button id="btn2"  class="btn btn-success type="button">Subir</button>')
            }
        }
     });
}

function borrarImg(aux, img, id){
    if (aux=='aux0'){
        nameImg = $('#aux0').val();
    }
    if (aux=='aux1'){
        nameImg = $('#aux1').val();
    }
    if (aux=='aux2'){
        nameImg = $('#aux2').val();
    }
    data = {
        "nameImg" : nameImg,
        "img" : img,
        "id" : id
    }

    $.ajax({
        data: data,
        url: '../modelos/coches/delete_img.php',
        type: 'POST',
        success: function (response) {
            car(id);
        }
    });
}
function upImg(num){
    var idCar = $('#idCar').val()
    if(num == 1){
        var inputFile = $("#img4")[0];
        img = "img0";
    }
    if(num == 2){
        var inputFile = $("#img5")[0];
        img = "img1";
    }
    if(num == 3){
        var inputFile = $("#img6")[0];
        img = "img2";
    }

    var data = new FormData();
    data.append('imgFile', inputFile.files[0]);
    data.append('img', img);
    data.append('idCar', idCar);

    if(num==1){
        $.ajax({
            data: data,
            url: '../modelos/coches/updateImg.php',
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (response) {
                location.reload();
            }
        });
    }
    if(num==2){
        $.ajax({
            data: data,
            url: '../modelos/coches/updateImg1.php',
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (response) {
                location.reload();
            }
        });
    }
    if(num==3){
        $.ajax({
            data: data,
            url: '../modelos/coches/updateImg2.php',
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (response) {
                location.reload();
            }
        });
    }

}