$(function () {
    listaCoches();

    $('#seleccionarCoche').change(function () {
        var idCoche = $('#seleccionarCoche').val();
        imagenCoche(idCoche);
        $('.imagenCoche').empty();
    });

    $('#btnGuardarDatosCocheMantenimiento').click(function () {
        insertarDatosCocheMantenimiento();
    });

    $('#btnHistorialMantenimientoCoche').click(function () {
        $("#datatable").dataTable().fnDestroy();
        $('#table').empty();
        historialMantenimientoCoche();
    })
});

function listaCoches() {
    $.ajax({
        url: '../modelos/coches/list_cars.php',
        success: function (data) {
            $.each(JSON.parse(data), function (k, v) {
                $('<option>').val(v.id).text(v.marca + " " + v.modelo).appendTo('#seleccionarCoche');
            });
        }
    })
}

function imagenCoche(id) {
    $.ajax({
        data: {id: id},
        url: '../modelos/coches/view_car.php',
        success: function (data) {
            $.each(JSON.parse(data), function (k, v) {
                var imagenCoche = "<div class='mt-5 imagenCoche'>" +
                    "<img class='mx-auto d-block' src=../../img/" + v.img0 + ">"
                    + "</div>";
                $('#left').append(imagenCoche);
            })
        }
    })
}

function insertarDatosCocheMantenimiento() {
    var idCoche = $('#seleccionarCoche').val();
    var importe = $('#importe').val();
    var fecha = $('#fechaMantenimiento').val();
    var taller = $('#taller').val();
    var descripcion = $('#descripcion').val();

    if ($.isNumeric(idCoche) && $.isNumeric(importe) && $.trim(fecha).length > 0 && $.trim(taller).length > 0
        && $.trim(descripcion).length > 0) {
        $.ajax({
            type: 'POST',
            url: '../modelos/coches/insertarDatosCocheMantenimiento.php',
            data: {idCoche: idCoche, importe: importe, fecha: fecha, taller: taller, descripcion: descripcion},
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON === true) {
                    $.confirm({
                        icon: 'far fa-check-circle',
                        title: '¡Datos guardados!',
                        content: 'Sus datos se han guardado.',
                        type: 'green',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-green',
                                action: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                } else {
                    $.confirm({
                        icon: 'fas fa-exclamation-triangle',
                        title: '¡Datos no guardados!',
                        content: 'Los datos no se ha podido guardar',
                        type: 'orange',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-orange',
                                action: function () {
                                    location.reload();
                                }
                            }
                        }
                    });
                }
            }
        })
    } else {
        $.confirm({
            icon: 'fas fa-exclamation-triangle',
            title: '¡Campos vacíos!',
            content: 'Por favor rellena todos los campos',
            type: 'orange',
            theme: 'modern',
            buttons: {
                Aceptar: {
                    btnClass: 'btn-orange',
                    action: function () {
                        location.reload();
                    }
                }
            }
        });
    }
}

function historialMantenimientoCoche() {
    var idCoche = $('#seleccionarCoche').val();
    if ($.isNumeric(idCoche)) {
        $.ajax({
            url: '../modelos/coches/selectMantenimientoCoche.php',
            data: {idCoche: idCoche},
            success: function (data) {
                var dataJSON = JSON.parse(data);
                if (dataJSON === false) {
                    $.confirm({
                        icon: 'fas fa-exclamation-triangle',
                        title: '¡Sin historial!',
                        content: 'Este modelo de coche todavía no tiene historial de mantenimiento.',
                        type: 'orange',
                        theme: 'modern',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn-orange'
                            }
                        }
                    });
                } else if (dataJSON.length > 0) {
                    for (var i = 0; i < dataJSON.length; i++) {
                        $('#table').append(
                            '<tr>' +
                            '<td>' + dataJSON[i].fecha + '</td>' +
                            '<td>' + dataJSON[i].taller + '</td>' +
                            '<td>' + dataJSON[i].importe + '</td>' +
                            '<td>' + dataJSON[i].descripcion + '</td>' +
                            '</tr>'
                        );
                    }
                    //Plugin de jQuery DataTable
                    table = $('#datatable').dataTable({
                        "columnDefs": [
                            {"title": "Fecha", "targets": 0},
                            {"title": "Taller", "targets": 1},
                            {"title": "Importe", "targets": 2},
                            {"title": "Descripcion", "targets": 3}
                        ]
                    });
                    $('#historialMantenimientoCocheModal').modal('show');
                }
            }
        })
    } else {
        $.confirm({
            icon: 'fas fa-exclamation-triangle',
            title: '¡Coche no seleccionado!',
            content: 'Por favor selecciona un coche.',
            type: 'orange',
            theme: 'modern',
            buttons: {
                Aceptar: {
                    btnClass: 'btn-orange'
                }
            }
        });
    }
}