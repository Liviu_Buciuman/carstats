  $(document).ready(function(){

    $( "#btnLogin" ).click(function() {
      check()
    });

    
    $( "#btnOut" ).click(function() {
      out()
    });

    //Plugin para slider 
    jQuery.backstretch([
    "../../img/slider2.jpg" //ruta de la segunda imagen
    , "../../img/slider3.jpg" //ruta de la tercera imagen
    , "../../img/slider4.jpg" //ruta de la cuarta imagen
    ], {duration: 4000, fade: 1000}
    );

});

function check(){

  var user = $('#nom').val();
  var pw = $('#pass').val();

  data = {
    "username": user,
    "password": pw
  }
  $.ajax({
      data: data,
      url: '../modelos/usuarios/checklogin.php',
      type: 'POST',
      success: function (response) {
        var status = JSON.parse(response);
        if(status){
          $('#ModalLogin').modal('toggle');
          $.confirm({
            title: 'Login correcto!',
            content: '',
            theme: 'black',
            type: 'orange',
            buttons: {
                Ok: function () {
                },
            }
        });
        }else{
          $('#error').html("Datos incorrectos");
          $("#error").css("color", "red");
        }

      }
    });
}

function out(){
  $.ajax("../modelos/usuarios/destroySession.php");
  $.confirm({
    title: 'Hecho!',
    content: 'Sesion cerrada',
    theme: 'black',
    type: 'orange',
    buttons: {
        Ok: function () {
        },
    }
});

}