$(document).ready(function () {

    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    $('#userID').val(unescape(value[1]))
    user(unescape(value[1]));

    listcar();
    
    $('#btnOk').click(function () {
        check();
    });

    $('select').on('change', function() {
        check();
    })

    //Muestra el formulario de dos conductores, cambiar el icono del del boton. Ademas llama la funcion de calcular el precio
    $('#btnTwo').val("1");
    $('#btnTwo').click(function () {
        $('#drivers').toggle();
        if($('#btnTwo').val()=="2"){
            $('#btnTwo').html('<i class="fas fa-user"></i><i class="fas fa-user"></i>');
            $('#btnTwo').val("1");
            calculate();
        }else{
            $('#btnTwo').html('<i class="fas fa-user"></i>');
            $('#btnTwo').val("2");
            calculate();
        }

    });
    //Llamo a la funcion de disponibilidad con el valor del coche
    $('#ava').click(function () {
        availability($('option:selected').val());
    });
    //Le digo al input tipo date que no permita marcar dias anteriores al actual
    $('[type="date"]#start').prop('min', function(){
        return new Date().toJSON().split('T')[0];
    });
    $('[type="date"]#end').prop('min', function(){
        return new Date().toJSON().split('T')[0];
    })
    //Si elige o cambia el seguro llamo a la funcion para calcular el precio
    $('input[name=seguro]').change(function() {
        calculate();
    });

});

//Llamo los datos del usuario
function user(id) {
    data = {
        "id": id
    }
    $.ajax({
        data: data,
        url: '../modelos/usuarios/view_user.php',
        type: 'GET',
        success: function (response) {
            var user = JSON.parse(response);
            $("#nomUser").html(user[0].nombre + ' ' + user[0].apellidos);
            $("#dni").append(' ' + user[0].dni);
            $("#telefono").append(' ' + user[0].telefono);
            $("#email").append(' ' + user[0].email);
        }
    })
}

function listcar() {
    //Llamada ajax para cargar los coches en el select
    $.ajax("../modelos/coches/list_cars.php", {
        success: function (response) {
            var car = JSON.parse(response);
            for (var i = 0; i < car.length; i++) {
                $('#car').append(
                    '<option value=' + car[i].id + '>' + car[i].marca + '-' + car[i].modelo + '</option>'
                );

            }
        }
    });
}


//Combruebo que el coche este disponible o no
function check() {

    var carId = $('option:selected').val();
    var start = $('#start').val();
    var end = $('#end').val();

    data = {
        "id": carId,
        "start" : start,
        "end" : end,
    }
    
    if(carId.length>0 && start.length>0 && end.length>0){
        if(start<end){
        $.ajax({
            data: data,
            url: '../modelos/coches/checkCarForService.php',
            type: 'GET',
            success: function (response) {
                var car = JSON.parse(response);
                if(car>0){
                    $('#not').html("Reservado");
                    $('#not').show();
                    $('#yes').hide();
                    $('#booking').hide();  
                }else{
                    //Si no esta reservado calculo el precio del alquiler
                    $.ajax({
                        data: data,
                        url: '../modelos/coches/view_car.php',
                        type: 'GET',
                        success: function (response) {
                            var price = JSON.parse(response);
                            var startDate = new Date(start);
                            var endDate = new Date(end);
                            var dif = endDate-startDate;
                            var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
                            var priceF = price[0].precio*dias;
                            $('#booking').show();  
                            $('#booking').html('<button id="btnInitial" class="btn btn-success m-3"  onClick="continueBooking(' + dias + ')"> <i class="fas fa-arrow-alt-circle-right"></i> Precio inicial: '+ priceF +' €</button>');
                            $('#booking').val(priceF);
                            calculate();
                        }
                    });
                    $('#yes').show();
                    $('#not').hide();
                }
            }
        });
    }else{
        $('#not').show();
        $('#not').html("Rellene las fechas correctamente");
        $('#booking').hide();  
        $('#yes').hide();

    }
    }else{
         $('#not').show();
         $('#not').html("Rellena los campos");
    }

}

function  availability(idCar){
    $('#tableDate').empty();

    if(idCar!=""){
        data = {
            "id" : idCar
        }
        $.ajax({
            data : data,
            url : '../modelos/coches/availability.php',
            type : 'GET',
            success: function (response) {
                var dates = JSON.parse(response);
                for (var i = 0; i < dates.length; i++) {
                    //Cambio el formato de mecha a uno mas amable
                    var fecha = new Date(dates[i].fechainicio);
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                    var start = fecha.toLocaleDateString("es-ES", options);

                    var fecha2 = new Date(dates[i].fechafin);
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                    var end = fecha2.toLocaleDateString("es-ES", options);

                    $('#tableDate').append(
                        '<tr>' +
                            '<td>' + start + ' </td>'+
                            '<td> - </td>'+
                            '<td>' + end + '</td>'+
                        '</tr>'
                    );
                }             
            }
        });
        $('#ModalAva').modal('toggle');
    }else{
        $.confirm({
            title: 'Hecho!',
            content: 'Pago editado',
            theme: 'black',
            type: 'orange',
            buttons: {
                Ok: function () {
                },
            }
        });    
    }
}

function continueBooking(dias){
    $('#extras').show();
    var priceBasic;
    var priceFull;

    $.ajax("../modelos/coches/extras.php", {
        success: function (response) {
            var extras = JSON.parse(response);
            for (var i = 0; i < extras.length; i++) {
                if(extras[i].id==1){
                    priceB = extras[i].precio;
                    $('#basic').val(extras[i].id);
                }
                if(extras[i].id==2){
                    priceF = extras[i].precio;
                    $('#full').val(extras[i].id);
                }
            }
            var priceBasic = priceB*dias
            var priceFull = priceF*dias

            $('#s1').html('Seguro basico por ' + dias + ' dias ' + (priceBasic) + ' €');
            $('#s2').html('Seguro premium por ' + dias + ' dias ' + (priceFull) + ' €');        

            $('#priceB').val(priceBasic);
            $('#priceF').val(priceFull);
            $('#driver').show();

        }
    });

}

function calculate(){
    var driver = parseInt($('#btnTwo').val()); //Numero de conductores 
    var priceI =  parseInt($('#booking').val()); //Precio inicial
    var insurance = parseInt($('input[name=seguro]:checked', '#insurance').val()); //Tipo de seguro

    //Verifico si ha marcado el seguro
    if(insurance){
        if(insurance=='1'){
            aux = parseInt($('#priceB').val());
        }
        if(insurance=='2'){
            aux = parseInt($('#priceF').val());
        }
        $('#choose').empty();
        //Calculamos el precio final

        if(driver=='2'){
            finalDriver = parseInt(200);
        }else{
            finalDriver = parseInt(0);
        }

        final = (finalDriver) + (aux) + (priceI);

        $('#PriceCalculate').html('<button id="btnPriceCalculate" onClick="reservation()" class="btn btn-success m-3" > <i class="fas fa-arrow-alt-circle-right"></i> Reservar y pagar por : ' +  final + '€</button>')
        $('#PriceCalculate').val(final);
    }else{
        $('#choose').html("Selecciona un seguro")
        $("#choose").css("color", "red");
    }
}

function reservation(){
    var ok;
    var ok2;
    var driver = $('#btnTwo').val(); //Numero de conductores 

    var typePay =$('input[name=gopay]:checked', '#pay').val(); //Tipo de pago

    if(typePay){
        ok = true;
    }else{
        ok =false;
    }

    //Compruebo que el formulario de un solo conductor este rellenado
    if(driver==1){
        if($('#nam0').val()!="" && $('#dni0').val()!="" && $('#cad0').val()!=""){
            ok2 = true;
            $('#all').empty();
        }else{
            ok2 = false;
            $('#all').html("Rellene todos los campos")
            $("#all").css("color", "red");
        }
    }

    //Compruebo que el formulario de dos conductores este rellenado
    if(driver=='2'){
        if($('#nam0').val()!="" && $('#dni0').val()!="" && $('#cad0').val()!="" && $('#nam1').val()!="" && $('#dni1').val()!="" && $('#cad1').val()!="" ){
            ok2 = true;
            $('#all2').empty();
            $('#all').empty();
        }else{
            ok2 = false;
            $('#all2').html("Rellene todos los campos")
            $("#all2").css("color", "red");
        }
    }

    //Conpruebo que haya elegido un metodo de pago
    if(typePay){
        ok = true;
        $('#choosePay').empty();

    }else{
        ok = false;
        $('#choosePay').html("Selecciona un seguro")
        $("#choosePay").css("color", "red");
    }

    if(ok && ok2){
        var userId = $('#userID').val() // ID del seguro
        var carId = $('option:selected').val(); //ID del coche
        var start = $('#start').val(); // Fecha inicial
        var end = $('#end').val();  // Fecha final
        var insurance = parseInt($('input[name=seguro]:checked', '#insurance').val()); //Tipo de seguro
        var typePay = $('input[name=gopay]:checked', '#pay').val();  //Tipo de pago
           
        dataService = {
            "type" : $('input[name=gopay]:checked', '#pay').val(),
            "priceFinal" :  $('#PriceCalculate').val(),
            "idUser" : userId,
            "idCar" : carId,
            "start" : start,
            "end" : end,
            "insurance" : insurance,
            //Datos conductores
            "driver" : driver,
            "nam0" : $('#nam0').val(),
            "dni0" : $('#dni0').val(),
            "cad0" : $('#cad0').val(),
            "nam1" : $('#nam1').val(),
            "dni1" : $('#dni1').val(),
            "cad1" : $('#cad1').val()
        }
        $.ajax({
            data : dataService,
            url : '../modelos/facturas/addService.php',
            type : 'POST',
            success: function (response) {
                $.confirm({
                    title: 'Hecho!',
                    content: 'Servicio contratado',
                    theme: 'black',
                    type: 'orange',
                    buttons: {
                        Ok: function () {
                            window.location = "bills.php";
                        },
                    }
                });
            }
        });

        $.ajax({
            data : dataService,
            url : '../modelos/facturas/enviarCorreo.php',
            type : 'POST',

        });


    }
}