<?php
    include_once('db.php');
    
    global $enlace;   
             
    $date = date('Y-m-d');
    $sql = "SELECT c.dni, c.nombre, c.apellidos, c.telefono, s.fechainicio, s.fechafin, s.fechaReserva , f.estado, p.tipoDePago, p.importe FROM cliente as c 
            JOIN servicio_alquiler AS s ON c.id=s.idcliente 
            JOIN factura AS f ON f.idServicio=s.id 
            JOIN pago as p ON p.idFactura=f.id 
            WHERE CAST(s.fechaReserva AS DATE)='$date'";
    $result = $enlace->query($sql);
    $data = "";
    $exportPath = "\wamp\www\carstats\cvs_facturas";
    $inpFileName = $date.".csv";
    $data= "DNI, Nombre, Apellidos, Telefono, Fecha Inicio, Fecha fin, Estado, Tipo de pago, Fecha de reserva, Importe".CHR(13);
    /*If file already found delete it from directory*/
    try {
        if(file_exists($exportPath."/".$inpFileName)) {
            unlink($exportPath."/".$inpFileName);
        }
    }catch(Exception $e){
        echo $e->getMessage();
    }
    $fh = fopen($exportPath."/".$inpFileName, "a") or die("can not open file");
    fwrite($fh, $data);
    while($row = $result->fetch_array()) {
        $data =  $row["dni"].",".$row["nombre"].",".$row["apellidos"].",".$row["telefono"].",".$row["fechainicio"].",".$row["fechafin"].",".$row["estado"].",".$row["tipoDePago"].",".$row["fechaReserva"].",".$row["importe"].CHR(13);
        fwrite($fh, $data);
    }
    fclose($fh);
    
?>


