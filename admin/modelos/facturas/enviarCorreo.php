<?php
    include_once('db.php');
    require_once 'PHPMailerAutoload.php';
    
    global $enlace;
    mysqli_set_charset($enlace, 'utf8');

    $idUser = $_POST['idUser'];
    $idCar = $_POST['idCar'];
    $start = $_POST['start'];
    $end = $_POST['end'];
    $insurance = $_POST['insurance'];
    $price = $_POST['priceFinal'];
    $type = $_POST['type'];

    $result =  $enlace->query("SELECT email, nombre FROM cliente WHERE id ='$idUser'");
    if ($result) { 
        while ($row = mysqli_fetch_assoc($result)){
            $email =$row['email']; 
            $nombre =$row['nombre']; 

        }
    }
    $result2 =  $enlace->query("SELECT marca, modelo FROM coche WHERE id ='$idCar'");
    if ($result2) { 
        while ($linea = mysqli_fetch_assoc($result2)){
            $marca = $linea['marca']; 
            $modelo = $linea['modelo']; 
        }
    }

    $mensajeEnviado = "";
    
    $body = "Gracias por su reserva ". $nombre;
    $body .= "Marca:".$marca ."<br> Modelo:".$modelo."<br> Fecha inicio:".$start."<br> Fecha fin:".$end;
    $body .= "Precio final:".$price;

    try {

        $mail = new PHPMailer(); // create a new object

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->IsHTML(true);
        $mail->Username = "carstatspuente@gmail.com";
        $mail->Password = "CarStats";
        $mail->setFrom("carstatshelp@gmail.com", "CarStats");
        $mail->addReplyTo("carstatshelp@gmail.com", "CarStats");
        $mail->Subject = "Reserva CarStats";
        $mail->Body = $body;
        $mail->AddAddress($email);
        $mensajeEnviado = true;
        $mail->send();
    } catch (phpmailerException $e) {
        $mensajeEnviado = false;
    }
    echo json_encode($mensajeEnviado, JSON_UNESCAPED_UNICODE);