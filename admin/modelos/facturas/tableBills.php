<?php
    include_once('db.php');
    
    global $enlace;

    mysqli_set_charset($enlace, 'utf8');
    $date1 = date('Y-m-d');
    $add_days = 30;
    $dateStart = date('Y-m-d',strtotime($date1) - (24*3600*$add_days));


    $result =  $enlace->query("SELECT car.marca, car.modelo, c.dni, c.nombre, c.apellidos, s.fechaInicio, s.fechaFin , f.estado, f.id, p.importe FROM cliente as c 
        JOIN servicio_alquiler AS s ON c.id=s.idcliente 
        JOIN factura AS f ON f.idServicio=s.id 
        JOIN pago as p ON p.idFactura=f.id 
        JOIN coche AS car ON  s.idCoche=car.id
        WHERE s.fechaInicio>='$dateStart'");
    $bills = array();
    $bills = $result->fetch_all(MYSQLI_ASSOC);

    echo json_encode( $bills , JSON_UNESCAPED_UNICODE );
    
?>