<?php
    include_once('db.php');
    
    global $enlace;

    mysqli_set_charset($enlace, 'utf8');

    $result =  $enlace->query("SELECT * FROM extra");

    $extras = array();
    $extras = $result->fetch_all(MYSQLI_ASSOC);

    echo json_encode( $extras , JSON_UNESCAPED_UNICODE );
    

?>