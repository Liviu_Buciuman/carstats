<?php
    include_once('db.php');

    global $enlace;

    mysqli_set_charset($enlace, 'utf8');

    $result =  $enlace->query("SELECT * FROM coche");

    $cars = array();
    $cars = $result->fetch_all(MYSQLI_ASSOC);

    echo json_encode( $cars , JSON_UNESCAPED_UNICODE );
    
?>