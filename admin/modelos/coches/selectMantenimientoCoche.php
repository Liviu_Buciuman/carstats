<?php
$host = "localhost";
$username = "root";
$passwd = "";
$dbname = "carstatsdb";

$conn = new mysqli("$host", "$username", "$passwd", "$dbname");
mysqli_set_charset($conn, "utf8");

$idCoche = $_GET["idCoche"];

$query = $conn->query("SELECT * FROM mantenimiento_coche WHERE idCoche = '$idCoche'");

$lista = array();
if ($query->num_rows > 0) {
    $lista = $query->fetch_all(MYSQLI_ASSOC);
} else {
    $lista = false;
}

echo json_encode($lista, JSON_UNESCAPED_UNICODE);