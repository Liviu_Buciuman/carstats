<?php

session_start();

if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != true) {

    $newURL = '../index.php';

    header('Location: '.$newURL);

}
