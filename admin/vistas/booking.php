<?php 
    include 'header.php'; 
    include '../modelos/usuarios/session.php';    
?>
<script src="../scripts_JS/booking.js"></script>

<body>
    <div class="container">
    <div class="p-5" id="userID"></div>
        <div class="row">
        
            <div class="col-md-5 col-lg-5 col-xs-12 col-sm-12">
                <div class="card text-center col-xs-12">
                    <img class="card-img-top" src="../../img/slider4.jpg" alt="Card image cap">
                    <!-- List Group -->
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">            
                            <h4><div class="mb-2" id=nomUser></div></h4>
                            <div id="telefono"><i class="fas fa-phone"></i></div>
                            <div id="dni"><i class="fas fa-id-card"></i></div>
                            <div id="email"><i class="fas fa-envelope" style></i></div>
                        <li class="list-group-item">Datos personales</li>
                    </ul>
                </div>
            </div>

            <div class="offset-md-1 offset-lg-1 col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <h3 class="mt-4">CarStats</h3>
                <div class="row">
                    <select class="custom-select col-md-6 col-lg-6 col-xs-12 col-sm-12 m-3" id="car">
                        <option value="">Selecciona coche...</option>
                    </select>
                    <button class="btn btn-dark m-3" id="ava">Reservas</button>

                </div>
                <label for="start">Inicio</label>
                <input type="date" class="form-control col-md-6 col-lg-6 col-xs-12 col-sm-12" id="start">
                <label for="start" class="mt-3">Fin</label>
                <input type="date" class="form-control col-md-6 col-lg-6 col-xs-12 col-sm-12" id="end">
                <div class="row">
                    <button class="btn btn-dark m-3" id="btnOk">Aceptar</button>
                    <div id="booking"></div>
                    <input type="hidden" id="priceI" />
                </div>
                <div class="alert alert-danger" role="alert" id="not"  style="display:none">                
                </div>
                <div class="alert alert-success" role="alert" id="yes"  style="display:none">
                    Libre! Go!!!
                </div>
            </div>
        </div>

        <div id="extras" style="display:none">
            <div><h3 class=" border-bottom border-secondary mt-5">Extras</h3></div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 mt-3">
                    <form id="insurance">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="seguro" value="basic" id="basic">
                            <input type="hidden" id="priceB">
                            <label class="form-check-label" for="basic" id="s1"></label>
                        </div>
                        <div class="form-check form-check-inline mt-3">
                            <input class="form-check-input" type="radio" name="seguro" value="full" id="full">
                            <input type="hidden" id="priceF">
                            <label class="form-check-label" for="full" id="s2"></label>
                        </div>
                    </form>
                </div>
            </div>
            <div id="choose"></div>

            <div><h3 class=" border-bottom border-secondary mt-5">Conductor/es (Conductor extra + 200€)</h3></div>

            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 mt-3">
                <button class="btn btn-dark m-3" val="2" id="btnTwo"><i class="fas fa-user"></i><i class="fas fa-user"></i></button>
            </div>

            <div class="form-row ">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <label for="nam0" class="col-form-label col-form-label-sm">Nombre</label>
                    <input type="text" class="form-control col-form-label-sm" id="nam0" required>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <label for="dni0" class="col-form-label col-form-label-sm">DNI</label>
                    <input type="text" class="form-control col-form-label-sm" id="dni0" required>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <label for="cad0" class="col-form-label col-form-label-sm">Caducidad carnet de conducir</label>
                    <input type="date" class="form-control col-form-label-sm" id="cad0" required>
                </div>
            </div>
            <div id="all"></div>

            <!-- Inputs para 2 conductores -->
            <div class="mb-5" id="drivers" style="display:none">
                <div class="form-row ">
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label for="nam1" class="col-form-label col-form-label-sm">Nombre 2</label>
                        <input type="text" class="form-control col-form-label-sm" id="nam1" required>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <label for="dni1" class="col-form-label col-form-label-sm">DNI</label>
                        <input type="text" class="form-control col-form-label-sm" id="dni1" required>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <label for="cad1" class="col-form-label col-form-label-sm">Caducidad carnet de conducir</label>
                        <input type="date" class="form-control col-form-label-sm" id="cad1" required>
                    </div>
                </div>
                <div id="all2"></div>
            </div>

            <div><h3 class=" border-bottom border-secondary mt-5">Tipo de pago</h3></div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 mt-3">
                    <form id="pay">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gopay" value="card" id="creditCard">
                            <label class="form-check-label" for="basic" id="creditCard">Tarjeta</label>
                        </div>
                        <div class="form-check form-check-inline mt-3">
                            <input class="form-check-input" type="radio" name="gopay" value="trans" id="Transf">
                            <label class="form-check-label" for="full" id="Transf">Transferencia bancaria</label>
                        </div>
                    </form>
                    <div id="choosePay"></div>
                    <div id="PriceCalculate"></div>
                </div>
            </div>
        </div>

        <!--Modal Fechas-->
        <div class="modal fade" id="ModalAva" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLongTitle">
                        <i class="fas fa-calendar-alt"></i> Fechas de reserva </h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table" id="tableDate">
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>

<?php include 'footer.html'; ?>
