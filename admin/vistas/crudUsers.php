<?php 
    include 'header.php'; 
    include '../modelos/usuarios/session.php';
?>
<script src="../scripts_JS/crudUsers.js"></script>
<body>
    <main>
        <div class="container">
            <div class="p-5"></div>
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#ModalAdd"><i class="fas fa-user-plus"></i> Crear usuario</button>
            <div class="p-3"></div>
            <!-- Modal boostrap para agregar usuarios nuevos-->
            <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-user-plus"></i> Nuevo usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form>
                       <div class="form-group">
                           <div class="row">
                               <div id="iduser" style="display:none"></div>
                               <div class="col">
                                   <label for="dni" class="col-form-label col-form-label-sm">DNI</label>
                                   <input type="text" class="form-control col-form-label-sm" id="dni"
                                          placeholder="12345678A"
                                          required>
                               </div>
                               <div class="col">
                                   <label for="nombre" class="col-form-label col-form-label-sm">Nombre</label>
                                   <input type="text" class="form-control col-form-label-sm" id="nombre"
                                          placeholder="Juan Manuel"
                                          required>
                               </div>
                           </div>
                           <label for="apellidos" class="col-form-label col-form-label-sm">Apellidos</label>
                           <input type="text" class="form-control col-form-label-sm" id="apellidos"
                                  placeholder="Florez Vasco" required>
                           <div class="row">
                               <div class="col">
                                   <label for="pass" class="col-form-label col-form-label-sm">Contraseña</label>
                                   <input type="password" class="form-control col-form-label-sm" id="pass"
                                          placeholder="Contraseña" required>
                               </div>
                               <div class="col">
                                   <label for="tel" class="col-form-label col-form-label-sm">Teléfono</label>
                                   <input type="text" class="form-control col-form-label-sm" id="tel"
                                          placeholder="933 07 34 02"
                                          required>
                               </div>
                           </div>
                           <label for="dire" class="col-form-label col-form-label-sm">Dirección</label>
                           <input type="text" class="form-control col-form-label-sm" id="dire"
                                  placeholder="Carrer de la Selva de Mar, 211, 08020 Barcelona"
                                  required>
                           <label for="email" class="col-form-label col-form-label-sm">Correo electrónico</label>
                           <input type="email" class="form-control col-form-label-sm" id="email"
                                  placeholder="juan.manuel@gmail.com"
                                  required>
                            <div id="error" class="p-2"></div>
                       </div>
                   </form>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" id="btnNuevoUsuario">Aceptar</button>

                    </div>
                </div>
                </div>
            </div> 
            <!--Fin modal-->

            <!--Modal Editar-->
            <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        
                    <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-user-plus"></i> Editar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                    </div>
                    <div class="modal-body">
                    <form>
                       <div class="form-group">
                           <div class="row">
                               <div class="col">
                                   <label for="dni" class="col-form-label col-form-label-sm">DNI</label>
                                   <input type="text" class="form-control col-form-label-sm" id="dni2"
                                          placeholder="12345678A"
                                          required>
                               </div>
                               <div class="col">
                                   <label for="nombre" class="col-form-label col-form-label-sm">Nombre</label>
                                   <input type="text" class="form-control col-form-label-sm" id="nombre2"
                                          placeholder="Juan Manuel"
                                          required>
                               </div>
                           </div>
                           <label for="apellidos" class="col-form-label col-form-label-sm">Apellidos</label>
                           <input type="text" class="form-control col-form-label-sm" id="apellidos2"
                                  placeholder="Florez Vasco" required>
                           <div class="row">
                               <div class="col">
                                   <label for="tel" class="col-form-label col-form-label-sm">Teléfono</label>
                                   <input type="text" class="form-control col-form-label-sm" id="tel2"
                                          placeholder="933 07 34 02"
                                          required>
                               </div>
                           </div>
                           <label for="dire" class="col-form-label col-form-label-sm">Dirección</label>
                           <input type="text" class="form-control col-form-label-sm" id="dire2"
                                  placeholder="Carrer de la Selva de Mar, 211, 08020 Barcelona"
                                  required>
                           <label for="email" class="col-form-label col-form-label-sm">Correo electrónico</label>
                           <input type="email" class="form-control col-form-label-sm" id="email2"
                                  placeholder="juan.manuel@gmail.com"
                                  required>
                       </div>
                   </form>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnEditUsuario">Aceptar</button>

                    </div>
                </div>
                </div>
            </div> 
            <!--Fin modal-->

            <!--Tabla usuarios-->
            <div>
                <table id="datatable" class="display responsive nowrap">
                    <thead>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody id="table" > 
                    </tbody >
                </table>   
            </div>
        </div>
    </main>
</body>
</html>