<?php  
    include 'header.php'; 
    include '../modelos/usuarios/session.php';
?>
<script src="../scripts_JS/viewCar.js"></script>


<body>
    <div class="container">

        <div class="p-5"></div>
        <button type="button" onclick="goBack()" class="btn btn-dark">
            <i class="fas fa-chevron-circle-left"></i> volver</button>
        <div class="p-2" id="idCar"></div>

        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <table class="table">
                    <tr>
                        <th><h3 id="marca"></h3></th>
                    </tr>
                    <tr>
                        <th>Modelo</th>
                        <td><span  class="text-left" id="modelo"></span></td>
                    </tr>
                    <tr>
                        <th>Categoria</th>
                        <td id="categoria"></td>
                    </tr>
                    <tr>
                        <th>Motor</th>
                        <td id="motor"></td>
                    </tr>
                    <tr>
                        <th>Plazas</th>
                        <td id="plazas"></td>
                    </tr>
                    <tr>
                        <th>Puertas</th>
                        <td id="puertas"></td>
                    </tr>
                    <tr>
                        <th>Matricula</th>
                        <td id="matricula"></td>
                    </tr>
                    <tr>
                        <th>Transmision</th>
                        <td id="trans"></td>
                    </tr>
                    <tr>
                        <th>Precio</th>
                        <td id="precio"></td>
                    </tr>
                    <tr>
                        <td colspan="2" ><p id="descripcion"></p></td>
                    </tr>
                </table>
            </div>

            <div class="class col-md-6 col-lg-6 col-xs-12 col-sm-12 mt-5">
                <a href=""><img class="mb-2 cb" style="width:540px; high:273px;" id="img0" src="" tittle="Imagen portada"/></a> 
                                  
                <img class="cb" id="img2" tittle="Imagen coche 2"/>
                
                <form enctype="multipart/form-data">
                    <div id="aux0" class="input-group mb-3">
                        <div id="btn0" class="input-group-append">
                        </div>
                    </div>

                    <div id="aux1" class="input-group mb-3">
                        <div id="btn1" class="input-group-append">
                        </div>
                    </div>

                    <div id="aux2" class="input-group mb-3">
                        <div id="btn2" class="input-group-append">
                        </div>
                    </div>
                </form>
            </div>  

        </div>
    </div>     
</body>

<?php  
    include 'footer.html'; 
?>