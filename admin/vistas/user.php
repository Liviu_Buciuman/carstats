<?php  
    include 'header.php'; 
    include '../modelos/usuarios/session.php';
?>
<script src="../scripts_JS/viewUser.js"></script>
<body>
    <div class="container">
        <div class="p-5"></div>
        <button type="button" onclick="goBack()" class="btn btn-dark">
            <i class="fas fa-chevron-circle-left"></i> volver</button>
        <div class="p-2"></div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  mt-4">
            <div>
                <h3 id="nombre" class="panel-title ml-2"></h3>
            </div>

            <table class="table table-striped table-dark mt-3">
                <thead>
                    <tr>    
                        <th scope="col">#</th>        
                        <th scope="col">Apellidos</th>
                        <td id="apellido"></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>DNI</td>
                        <td id="dni"></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Telefono</td>
                        <td id="telefono"></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Email</td>
                        <td id="email"></td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>Dirección</td>
                        <td id="direccion"></td>
                    <tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>Fecha registro</td>
                        <td id="fecha"></td>
                    <tr>
                </tbody>
            </table>
            <button type="button" class="btn btn-dark btn-mini" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-envelope"></i> Contactar</button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  mt-5">
            <h3>Alquileres</h3>
            <div class="mt-5">
            <table id="datatable" class="display responsive nowrap">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="table" > 
                </tbody >
            </table> 
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <i class="fas fa-envelope"></i> Nuevo mensaje
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Mensaje:</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary">Enviar mensaje</button>
                </div>
            </div>
        </div>
    </div>

        
</body>
