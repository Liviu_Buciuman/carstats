<?php 
    include 'header.php'; 
    include '../modelos/usuarios/session.php';    
?>

<script type="text/javascript" src="../scripts_JS/mantenimientoCoche.js"></script>

<body>
<div class="container">
    <div class="row m-5 p-5 justify-content-center">
        <div id="left" class="col-md-12 col-lg-8 col-xs-12 col-sm-12">
            <div class="form-group">
                <label for="seleccionarCoche">Selecciona un coche:</label>
                <div class="input-group">
                    <select class="custom-select" id="seleccionarCoche">
                        <option selected value="none">Selecciona...</option>
                    </select>
                    <div class="input-group-append">
                        <button id="btnHistorialMantenimientoCoche" class="btn btn-outline-secondary">
                            <i class="fas fa-history"></i> Historial
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4 col-xs-12 col-sm-12">
            <form>
                <div class="form-group">
                    <label for="importe">Importe factura:</label>
                    <input type="number" id="importe" class="form-control"
                           placeholder="Introduzca el importe factura">
                </div>
                <div class="form-group">
                    <label for="fechaMantenimiento">Fecha mantenimiento:</label>
                    <input type="date" id="fechaMantenimiento" class="form-control">
                </div>
                <div class="form-group">
                    <label for="taller">Nombre taller:</label>
                    <input type="text" id="taller" name="taller" class="form-control" placeholder="Nombre taller">
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción:</label>
                    <textarea name="descripcion" id="descripcion" class="form-control" rows="4"
                              placeholder="Descripción avería..."></textarea>
                </div>
                <div class="row justify-content-center">
                    <div class="form-group">
                        <button type="button" id="btnGuardarDatosCocheMantenimiento" class="btn btn-primary">
                            Guardar datos
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal historial mantenimiento coche-->
    <div class="modal fade" id="historialMantenimientoCocheModal" tabindex="-1" role="dialog"
         aria-labelledby="historialModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="historialModalLabel"><i class="fas fa-history"></i> Historial mantenimiento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--Tabla historial mantenimiento coche-->
                    <div class="row">
                        <div id="divHistorialMantenimientoCoche" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-5">
                            <table id="datatable" class="display responsive nowrap">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
</body>

<?php require_once 'footer.html' ?>
