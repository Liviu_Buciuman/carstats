<?php 
    include 'header.php';
    include '../modelos/usuarios/session.php';
?>
<script src="../scripts_JS/crudCars.js"></script>


<body>
    <main>
        <div class="container">
            <div class="p-5"></div>
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#ModalAdd">
                <i class="fas fa-plus-circle"></i> Añadir coche</button>
            <div class="p-3"></div>
            
             <!--Tabla coches-->
             <div>
                <table id="datatable" class="display responsive nowrap">
                    <thead>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody id="table"> 
                    </tbody >
                </table>   
            </div>
            <!--final-->

            <!-- Modal boostrap para agregar coches nuevos-->
            <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">
                                <i class="fas fa-plus"></i> Nuevo coche</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="form-car" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="form-row ">
                                        <div id="idcoche" style="display:none"></div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="marca" class="col-form-label col-form-label-sm">Marca</label>
                                            <input type="text" class="form-control col-form-label-sm" id="marca" required>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="modelo" class="col-form-label col-form-label-sm">Modelo</label>
                                            <input type="text" class="form-control col-form-label-sm" id="modelo" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="categoria" class="col-form-label col-form-label-sm">Categoria</label>
                                            <input type="text" class="form-control col-form-label-sm" id="categoria" required>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="motor" class="col-form-label col-form-label-sm">Motor</label>
                                            <input type="text" class="form-control col-form-label-sm" id="motor" required>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="plazas" class="col-form-label col-form-label-sm">Plazas</label>
                                            <input type="text" class="form-control col-form-label-sm" id="plazas" required>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="puertas" class="col-form-label col-form-label-sm">Puertas</label>
                                            <input type="text" class="form-control col-form-label-sm" id="puertas" required>
                                        </div>
                                    </div>
                                    <div class="form-row ">
                                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                            <label for="matricula" class="col-form-label col-form-label-sm">Matricula</label>
                                            <input type="text" class="form-control col-form-label-sm" id="matricula" required>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                            <label for="trans" class="col-form-label col-form-label-sm">Transmision</label>
                                            <input type="text" class="form-control col-form-label-sm" id="trans" required>
                                        </div>
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                            <label for="precio" class="col-form-label col-form-label-sm">Precio</label>
                                            <input type="text" class="form-control col-form-label-sm" id="precio" required>
                                        </div>
                                    </div>
                                    <label for="img0" class="col-form-label col-form-label-sm">Imagen portada</label>
                                    <input type="file" accept="image/*" class="form-control col-form-label-sm" id="img0" required>
                                    <label for="img1" class="col-form-label col-form-label-sm">Imagen 1</label>
                                    <input type="file" accept="image/*" class="form-control col-form-label-sm" id="img1" required>
                                    <label for="img2" class="col-form-label col-form-label-sm">Imagen 2</label>
                                    <input type="file" accept="image/*" class="form-control col-form-label-sm" id="img2" required>
                                    <label for="descripcion" class="col-form-label col-form-label-sm">Descripcion</label>
                                    <textarea type="descripcion" class="form-control col-form-label-sm" id="descripcion" required></textarea>
                                    <div id="error" class="p-2"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-primary" id="btnNewCar">Aceptar</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
            <!--Fin modal-->

            <!-- Modal boostrap para editar coches-->
            <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">
                                <i class="fas fa-edit"></i> Editar coche</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="form-car" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="form-row ">
                                        <div id="id2" style="display:none"></div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="marca" class="col-form-label col-form-label-sm">Marca</label>
                                            <input type="text" class="form-control col-form-label-sm" id="marca2" required>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="modelo" class="col-form-label col-form-label-sm">Modelo</label>
                                            <input type="text" class="form-control col-form-label-sm" id="modelo2" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="categoria" class="col-form-label col-form-label-sm">Categoria</label>
                                            <input type="text" class="form-control col-form-label-sm" id="categoria2" required>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="motor" class="col-form-label col-form-label-sm">Motor</label>
                                            <input type="motor" class="form-control col-form-label-sm" id="motor2" required>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="plazas" class="col-form-label col-form-label-sm">Plazas</label>
                                            <input type="text" class="form-control col-form-label-sm" id="plazas2" required>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                            <label for="puertas" class="col-form-label col-form-label-sm">Puertas</label>
                                            <input type="puertas" class="form-control col-form-label-sm" id="puertas2" required>
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="matricula" class="col-form-label col-form-label-sm">Matricula</label>
                                            <input type="text" class="form-control col-form-label-sm" id="matricula2" required>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="trans" class="col-form-label col-form-label-sm">Transmision</label>
                                            <input type="text" class="form-control col-form-label-sm" id="trans2" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <label for="precio" class="col-form-label col-form-label-sm">Precio</label>
                                            <input type="text" class="form-control col-form-label-sm" id="precio2" required>
                                        </div>
                                    </div>

                                    <label for="descripcion" class="col-form-label col-form-label-sm">Descripcion</label>
                                    <textarea type="text" class="form-control col-form-label-sm" id="descripcion2" required></textarea>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" class="btn btn-primary" id="btnEditCar">Aceptar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!--Fin modal-->
        </div>
    </main>
</body>