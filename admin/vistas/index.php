<?php  
    include 'header.php'; 
?>

<script src="../scripts_JS/index.js"></script>


<body>
    <div class="container">
        <div id="center" class="col-lg-6 col-lg-offset-3 text-center">
            <div class="mt-5"></div>
            <button  id="myBtn" type="button" data-toggle="modal" data-target="#ModalLogin" class="btn btn-dark center-block mt-5">
            <i class="fas fa-sign-in-alt"></i> Login</button>
            <button  id="btnOut" type="button"class="btn btn-dark center-block mt-5">
            <i class="fas fa-times"></i> Salir</button>    
        </div>
 
    </div>   
</body>

<!-- Modal boostrap para login-->
<div class="modal fade" id="ModalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="fas fa-sign-in-alt"></i> Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div id="iduser" style="display:none"></div>
                        <div class="col">
                            <label for="dni" class="col-form-label col-form-label-sm">Usuario</label>
                            <input type="text" class="form-control col-form-label-sm" id="nom" placeholder="usuario" required>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col">
                            <label for="nombre" class="col-form-label col-form-label-sm">Nombre</label>
                            <input type="password" class="form-control col-form-label-sm" id="pass" placeholder="contraseña" required>
                        </div>
                    </div>
                    <div id="error" class="mt-2"></div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnLogin">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!--Fin modal-->